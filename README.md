# fail2ban-endlessh

**Utilizing _fail2ban_ and _endlessh_ together to take actions against those dog'gone pesky wannabe SSH hackersbotskidsscripties hooligans!!**

Git Repo: https://gitlab.com/DuckHP/fail2ban-endlessh

Licensed Under: http://www.wtfpl.net/about/


## - Requirements

* [fail2ban](https://github.com/fail2ban/fail2ban)
* [endlessh](https://github.com/skeeto/endlessh)
	- ‼️ NOTE: Endlessh's logging-level **must** be set to 'LogLevel 1' or else it's output linux will differ from what the 'failregex=' filter expects ‼️ 
* Optionally you might want some firewall like [shorewall](https://shorewall.org/index.html)** or [ufw](https://wiki.ubuntu.com/UncomplicatedFirewall).
---
** If using shorewall's dynamic blacklisting function, the ' BLACKLIST= ' config line should be set to ' BLACKLIST="ALL" ' in '/etc/shorewall/shorewall.conf' , or else it wont drop currently 'ESTABLISHED' connected bots but will only prevent 'NEW' reconnection.

See https://shorewall.org/blacklisting_support.htm#idm43 for more details on shorewall dynamic blacklist.

Also make sure to have a look trough the _/etc/fail2ban/action.d/shorewall.conf_ file that likely came with the _fail2ban_ install

---


## - Some links of possible interest

* https://odysee.com/@stembod:5/howto_troll_ssh_hackers_by_using_endlessh_tarpit:c
* https://github.com/fail2ban/fail2ban/tree/master/config/action.d
* https://rsabalburo.blogspot.com/2014/07/fail2ban-with-shorewall.html


## - Installation

Copy the filter file which will observe endlessh's log, and the action file which determines what to do about it:
``` bash
sudo cp jail.d/endlessh.local /etc/fail2ban/jail.d/
sudo cp filter.d/endlessh.local /etc/fail2ban/filter.d/
```

**Make sure to review and edit the two files according to need!**

Most notably enabling the jail by setting **'enabled= true'** and choosing **'banaction='** (it's likely set as 'banaction= shorewall' by default). Keep in mind that fail2ban is able to take multiple actions, so do check out _/etc/fail2ban/action.d/_ for possible options.

Test the configuration just to be sure:

``` bash
sudo fail2ban-server -t
```

If no errors, restart the fail2ban daemon:
``` bash
sudo systemctl restart fail2ban.service
```

* Check out https://regex101.com/r/gT8dSn/2 on how to adjust  'failregex=' filter if desirable.


## - How to monitor if it's working

Run the following to observe fail2ban
``` bash
watch sudo fail2ban-client get endlessh banip --with-time
```

Should keep outputting something similar to:
``` pre
Every 2,0s: sudo fail2ban-client get endlessh banip --with-time

122.194.229.40  2022-03-17 16:13:09 + 3600 = 2022-03-17 17:13:09
193.105.134.95  2022-03-17 16:34:56 + 3600 = 2022-03-17 17:34:56
195.3.147.60    2022-03-17 16:34:59 + 3600 = 2022-03-17 17:34:59
122.194.229.45  2022-03-17 16:44:29 + 3600 = 2022-03-17 17:44:29
61.177.173.2    2022-03-17 16:44:52 + 4531 = 2022-03-17 18:00:23
```

And in case you are using _shorewall_ as firewall, confirm they are being added (and auto-removed according to jail's 'bantime=' setting) by fail2ban in shorewall's dynamic blacklist:
``` bash
sudo shorewall show dynamic
```


## - Misc. Notes 

To list all the ip's currently in "endlessh jail" run:
``` bash
sudo fail2ban-client get endlessh banip
```

In case needing or wanting to manually purge shorewalls entire dynamic blacklist run:
``` bash
sudo shorewall show dynamic | tail -n+7 | cut -b48-62 | sudo xargs shorewall allow
```

To confirm fail2ban filter works with an endlessh log output:
``` bash
fail2ban-regex --print-all-matched \  
	regex_test_sample.log endlessh.local  

# Or in case some other logfile..

fail2ban-regex --print-all-matched \  
	/var/log/endlessh/endlessh.log endlessh
```
